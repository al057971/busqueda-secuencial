import java.util.Scanner;

/**
 *
 * @author Cristian Berzunza Poot
 */
public class Busec {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner sc = new Scanner(System.in);
        int[] arru = {45, 67, 78, 34, 13};
        int dato = 0;
        System.out.println("Ingrese el número a buscar");
        dato = sc.nextInt();
        
        busquedaSecuencial(arru, dato);
        int posicion = busquedaSecuencial(arru, dato);  
        if(posicion == 0){
           //se compara para que se sepa si se busco el dato o no 
           System.out.println("No se encontró el número");
        }else{
            //se imprimen los demas arreglos
            impdat(posicion);
        }
        
    }
    public static int busquedaSecuencial(int []arreglo,int dato){
    int posicion = 0;
    for(int i = 0; i < arreglo.length; i++){//recorremos todo el arreglo
      if(arreglo[i] == dato){//comparamos el elemento en el arreglo con el buscado
    posicion = i+1;//Si es verdadero guardamos la posicion
    break;//Para el ciclo
   }
     
 }
 return posicion;
}
    
    public static void impdat(int posicion){
    
        System.out.println("La posición es:   "+posicion);
        
        System.out.println("---------------------------------------");
    }
    
    
}
